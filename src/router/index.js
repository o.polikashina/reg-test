import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '../components/HomePage.vue';
import EmptyPage from '../components/EmptyPage.vue';
import ArticlePage from '../components/ArticlePage.vue';
import NotFoundPage from '../components/NotFoundPage.vue';

Vue.use(Router);

export default new Router({
    routes: [
        {path: '/', name: 'HomePage', component: HomePage},
        {path: '/about', name: 'AboutPage', component: EmptyPage},
        {path: '/contacts', name: 'ContactsPage', component: EmptyPage},
        {path: '/article/:id', name: 'ArticlePage', component: ArticlePage, props: true},
        {path: "*",  name: 'NotFoundPage', component: NotFoundPage }
    ]
});

import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state:{
        tiles:{},
        isReady: false
    },
    mutations:{
        updateTiles: (state, tiles) => {
            state.tiles = tiles.reduce((previousValue, currentValue, index, array) => {
                previousValue[currentValue.id] = currentValue;
                return previousValue;
            }, {});
            state.isReady = true;
        }
    },
    actions:{
        loadTiles ({commit}) {
            if (this.state.isReady) return;
            Axios.get('/api/tiles')
                .then(result => {commit('updateTiles', result.data.tiles)})
                .catch(error => console.log(error));
        }
    }
});
